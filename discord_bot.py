from main import answer_to_word,split_word
from os import getenv
import discord
from dotenv import load_dotenv
load_dotenv()

class DiBotBot(discord.Client):
    async def on_ready(self):
        print(f"Dibot (bot) ready (as {self.user})")

    async def on_message(self, message):
        if message.author.id == self.user.id: return

        for word in message.content.split(' '):
            if len(fragments := split_word(word)) > 1:
                await message.channel.send(answer_to_word(word))

intents = discord.Intents.default()
intents.message_content = True

bot = DiBotBot(intents=intents)
bot.run(getenv('DISCORD_TOKEN'))
