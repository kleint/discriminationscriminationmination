from archlinux:latest

run pacman -Syu --noconfirm espeak-ng python-poetry

copy pyproject.toml .
copy poetry.lock .

run poetry install

copy . .

cmd ["poetry", "run", "python", "discord_bot.py"]
