import re
from phonemizer import phonemize

wordbank = [
    "discrimination",
    "dislexie",
    "dynamique",
    "kryptonite",
    "dispraxie",
    "bondi",
]

# phonemizer = EspeakBackend("fr-fr")
# phonemizer.set_library('/lib/x86_84-linux-gnu/libespeak-ng.so.1')

phonemes = { "kʁi": "cri", "di": "di", "dju": "diu", "dja": "dia"}
literal = { "ʁ": "r", "ʃ":  "sh", "sjɔ̃":  "tion", "ɔ": "o", "ksi": "xi", "ɛ": "e", "ɑ̃": "an", "y":"u", "e": "é"}

def unpronounce(phonemes_str) -> str:
    print(f"Unpronouncing {phonemes_str}")
    for pronounced, written in (phonemes | literal).items():
        print(f"\t{written} -> {pronounced}")
        phonemes_str = phonemes_str.replace(pronounced, written)
    return phonemes_str


def remove_before(word, prefix) -> str:
    """

    >>> remove_before('test', 's')
    't'
    >>> remove_before('feur', 'ga')
    'feur'
    >>> remove_before('quoicoubaka', 'cou')
    'baka'
    """
    if prefix not in word:
        return word
    first_substring_index = word.find(prefix)
    return word[first_substring_index + len(prefix) :]


def is_closest_prefix(word, phoneme):
    """
    >>> pronounciation = phonemize('discrimination', language='fr-fr', strip=True)
    >>> is_closest_prefix(pronounciation, 'kʁi')
    False
    >>> is_closest_prefix(pronounciation, 'di')
    True

    >>> pronounciation = phonemize('didichamdidoui', language='fr-fr', strip=True)
    >>> pronounciation
    'didiʃamdidui'
    >>> is_closest_prefix(pronounciation, 'kʁi')
    False
    >>> is_closest_prefix(pronounciation, 'di')
    True
    >>> is_closest_prefix(remove_before(pronounciation, 'di'), 'di')
    True
    """

    positions = {
        p: word.find(p) if word.find(p) != -1 else 999999 for p in phonemes.keys()
    }
    return positions[phoneme] == min(positions.values())


def split_word(word) -> list[str]:
    result = [phonemize(word, language="fr-fr", strip=True)]

    uppercase_indices = []

    i = 0
    while result[-1]:
        pronounced = result[-1]
        if not any(p in pronounced for p in phonemes.keys()):
            break
        print(f"Iterating on {pronounced} ({result=})")
        for phoneme in phonemes.keys():
            print(f"Testing if {phoneme} is the best prefix within {pronounced}")
            if is_closest_prefix(pronounced, phoneme):
                if phoneme == "kʁi": uppercase_indices.append(i+1)
                fragment = remove_before(pronounced, phoneme)
                print(f"Adding {fragment} to {result}")
                result.append(fragment)
        i += 1
    return [w.upper() if j in uppercase_indices else w for (j,w) in enumerate(unpronounce(f) for f in result)]


def answer_to_word(word: str) -> str:
    return ", ".join(f + ("!" * i) for i, f in enumerate(split_word(word)[1:]))

if __name__ == "__main__":
    print(split_word('discrimination'))
