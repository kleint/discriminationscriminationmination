from flask import Flask, request

from main import split_word, answer_to_word

app = Flask(__name__)


@app.get("/<word>.json")
def _(word: str):
    return {"fragments": split_word(word)}


@app.get("/<word>.txt")
def __(word: str):
    return answer_to_word(word)

@app.get("/")
def _sentence():
    sentence = request.args.get("sentence") or ""
    for word in sentence.split(' '):
        fragments = split_word(word)
        if len(fragments) > 1:
            return __(word)
    return ""


app.run(debug=True)
